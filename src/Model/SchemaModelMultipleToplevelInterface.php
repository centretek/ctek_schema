<?php

namespace Drupal\ctek_schema\Model;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\ctek_schema\Schema\SchemaBase;

interface SchemaModelMultipleToplevelInterface {

  public function getSchema(CacheableMetadata|NULL $metadata = NULL): SchemaBase|array;

}
