<?php

namespace Drupal\ctek_schema\Model;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\ctek_schema\Schema\SchemaBase;

interface SchemaModelInterface {

  public function getSchema(CacheableMetadata $metadata) : SchemaBase;

}
