<?php

namespace Drupal\ctek_schema\Model;

use Drupal\Core\Render\BubbleableMetadata;

trait JsonLdModelTrait {

  public function getJsonLd(array $options, BubbleableMetadata $metadata) : string {
    if ( (!$this instanceof SchemaModelInterface) && (!$this instanceof SchemaModelMultipleToplevelInterface) ) {
      return '';
    }
    $schema = $this->getSchema($metadata);
    // If a single schema was returned, it can be built in the normal way.
    if (!is_array($schema)) {
      /** @var \Drupal\ctek_schema\Schema\SchemaBase $schema */
      return json_encode([
        '@context' => 'https://schema.org',
      ] + $schema->jsonSerialize(), JSON_PRETTY_PRINT);
    }
    // If there are multiple top-level schemas (e.g. WebPage and FAQPage),
    // they can defined as an array inside the @graph property.
    $graph = [];
    foreach ($schema as $s) {
      /** @var \Drupal\ctek_schema\Schema\SchemaBase $s */
      $graph[] = $s->jsonSerialize();
    }
    return json_encode([
      '@context' => 'https://schema.org',
      '@graph' => $graph,
    ], JSON_PRETTY_PRINT);
  }

}
