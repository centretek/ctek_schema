<?php

namespace Drupal\ctek_schema\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class SchemaGenerator extends Plugin {

  public $id;

  public $classNamespace;

  public $interfaceNamespace;

  public $classPath;

  public $interfacePath;

}
