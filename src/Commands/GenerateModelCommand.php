<?php

namespace Drupal\ctek_schema\Commands;

use Drupal\Core\File\FileSystemInterface;
use Drupal\ctek_schema\Schema\SchemaBase;
use Drupal\ctek_schema\Schema\SchemaGeneratorInterface;
use Drupal\ctek_schema\Schema\SchemaGeneratorPluginManager;
use Drush\Commands\DrushCommands;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\String\UnicodeString;

class GenerateModelCommand extends DrushCommands {

  protected $schemaGeneratorPluginManager;

  protected $fileSystem;

  public function __construct(
    SchemaGeneratorPluginManager $schemaGeneratorPluginManager,
    FileSystemInterface $fileSystem
  ) {
    parent::__construct();
    $this->schemaGeneratorPluginManager = $schemaGeneratorPluginManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * @command ctek_schema:generate
   */
  public function generate() {
    foreach ($this->schemaGeneratorPluginManager->getDefinitions() as $definition) {
      /** @var \Drupal\ctek_schema\Schema\SchemaGeneratorInterface $schemaGenerator */
      $schemaGenerator = $this->schemaGeneratorPluginManager->createInstance($definition['id']);
      $graph = new \EasyRdf_Graph();
      foreach ($schemaGenerator->getSchemaFiles() as $file) {
        $graph->parseFile($file);
      }
      $hierarchy = [];
      foreach ($schemaGenerator->getTargetClasses() as $uri => $properties) {
        /** @var \EasyRdf_Resource $type */
        $type = $graph->resource($uri);
        $this->gatherClasses($type, $hierarchy, $properties, $schemaGenerator);
      }
      $this->generateCode($hierarchy, $schemaGenerator);
    }
  }

  protected function gatherClasses(\EasyRdf_Resource $resource, array &$hierarchy, array $properties, SchemaGeneratorInterface $schemaGenerator) {
    if (count($hierarchy) === 0) {
      $hierarchy = [];
    }
    if (isset($schemaGenerator->getDataTypes()[$resource->getUri()])) {
      $hierarchy[$resource->getUri()] = [
        'type' => 'primitive',
        'uri' => $resource->getUri(),
        'name' => $resource->localName(),
        'phpType' => $schemaGenerator->getDataTypes()[$resource->getUri()],
      ];
      return;
    }
    if ($resource->type() !== 'rdfs:Class') {
      return;
    }
    $isTargetClass = isset($schemaGenerator->getTargetClasses()[$resource->getUri()]);
    if (!isset($hierarchy[$resource->getUri()])) {
      $hierarchy[$resource->getUri()] = [
        'type' => 'class',
        'uri' => $resource->getUri(),
        'name' => $resource->localName(),
        'parents' => [],
        'properties' => [],
        'needsConcrete' => $isTargetClass,
        'comment' => $resource->get('rdfs:comment'),
      ];
    }
    /** @var \EasyRdf_Resource $parent */
    foreach ($resource->all('rdfs:subClassOf') as $parent) {
      if (!in_array($parent->getUri(), $hierarchy[$resource->getUri()]['parents'])) {
        $hierarchy[$resource->getUri()]['parents'][] = $parent->getUri();
      }
      if (!isset($hierarchy[$parent->getUri()])) {
        $this->gatherClasses($parent, $hierarchy, $properties, $schemaGenerator);
      }
    }
    if (count($properties) === 0) {
      return;
    }
    /** @var \EasyRdf_Resource $property */
    foreach ($resource->getGraph()->allOfType('rdf:Property') as $property) {
      if (in_array($property->localName(), $properties)) {
        if (!isset($hierarchy[$property->getUri()])) {
          $rangeIncludes = [];
          /** @var \EasyRdf_Resource $range */
          foreach ($property->all('schema:rangeIncludes') as $range) {
            $rangeIncludes[] = $range->getUri();
            if (!isset($hierarchy[$range->getUri()])) {
              $this->gatherClasses($range, $hierarchy, $properties, $schemaGenerator);
            }
            $hierarchy[$range->getUri()]['needsConcrete'] = TRUE;
          }
          $hierarchy[$property->getUri()] = [
            'type' => 'property',
            'uri' => $property->getUri(),
            'name' => $property->localName(),
            'rangeIncludes' => $rangeIncludes,
            'comment' => $property->get('rdfs:comment'),
          ];
        }
        /** @var \EasyRdf_Resource $domain */
        foreach ($property->all('schema:domainIncludes') as $domain) {
          if ($domain->getUri() === $resource->getUri()) {
            $hierarchy[$resource->getUri()]['properties'][] = $property->getUri();
          }
        }
      }
    }
//    if ($isTargetClass) {
//      foreach ($properties as $property) {
//        if (isset($hierarchy[$resource->getUri()]['properties'][$property])) {
//          continue;
//        }
//        $propertyFound = FALSE;
//        foreach ($hierarchy[$resource->getUri()]['parents'] as $uri => $parent) {
//          if (isset($hierarchy[$uri]['properties'][$property])) {
//            $propertyFound = TRUE;
//            continue;
//          }
//        }
//        if ($propertyFound === FALSE) {
//          var_dump($resource->getUri());
//          var_dump($property);
//        }
//      }
//    }
  }

  protected function generateCode($hierarchy, SchemaGeneratorInterface $schemaGenerator) {
    foreach ($hierarchy as $type) {
      switch ($type['type']) {
        case 'class':
          $this->generateInterface($type, $hierarchy, $schemaGenerator);
          if ($type['needsConcrete']) {
            $this->generateClass($type, $hierarchy, $schemaGenerator);
          }
          break;
        case 'primitive':
          break;
      }
    }
  }

  protected function generateInterface($type, $hierarchy, SchemaGeneratorInterface $schemaGenerator) {
    $printer = new PsrPrinter();
    $interfaceName = $type['name'] . 'Interface';
    $file = new PhpFile();
    $namespace = $file->addNamespace($schemaGenerator->getInterfaceNamespace());
    $interface = $namespace->addClass($interfaceName);
    $interface
      ->setInterface();
    foreach ($type['parents'] as $parent) {
      if (isset($hierarchy[$parent])) {
        switch ($hierarchy[$parent]['type']) {
          case 'class':
            $interface->addExtend($schemaGenerator->getInterfaceNamespace() . '\\' . $hierarchy[$parent]['name'] . 'Interface');
            break;
          case 'primitive':

            break;
        }
      }
    }
    foreach ($type['properties'] as $uri) {
      $this->generateProperty($interface, $hierarchy[$uri], $hierarchy, $schemaGenerator);
    }
    $interfacePath = $schemaGenerator->getInterfacePath();
    $this->prepareDirectory($interfacePath);
    file_put_contents($interfacePath . '/' . $interfaceName . '.php', $printer->printFile($file));
  }

  protected function generateClass($type, $hierarchy, SchemaGeneratorInterface $schemaGenerator) {
    $printer = new PsrPrinter();
    $className = $type['name'];
    $interfaceName = $schemaGenerator->getInterfaceNamespace() . '\\' . $type['name'] . 'Interface';
    $file = new PhpFile();
    $namespace = $file->addNamespace($schemaGenerator->getClassNamespace());
    $namespace->addUse(SchemaBase::class);
    $class = $namespace->addClass($className);
    $class->addComment($this->formatComment($type['comment']));
    $class->addExtend(SchemaBase::class);
    $namespace->addUse($interfaceName);
    $class->addImplement($interfaceName);
    $this->generateParentProperties($class, $type['parents'], $hierarchy, $schemaGenerator);
    foreach ($type['properties'] as $uri) {
      $this->generateProperty($class, $hierarchy[$uri], $hierarchy, $schemaGenerator);
    }
    $classPath = $schemaGenerator->getClassPath();
    $this->prepareDirectory($classPath);
    file_put_contents($classPath . '/' . $className . '.php', $printer->printFile($file));
  }

  protected function prepareDirectory($directory) {
    if (!$this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $this->fileSystem->mkdir($directory, NULL, TRUE);
    }
  }

  protected function generateParentProperties(ClassType $type, array $parents, array $hierarchy, SchemaGeneratorInterface $schemaGenerator) {
    foreach ($parents as $parent) {
      if (isset($hierarchy[$parent])) {
        foreach ($hierarchy[$parent]['properties'] as $uri) {
          $this->generateProperty($type, $hierarchy[$uri], $hierarchy, $schemaGenerator);
        }
        $this->generateParentProperties($type, $hierarchy[$parent]['parents'], $hierarchy, $schemaGenerator);
      }
    }
  }

  protected function generateProperty(ClassType $type, $config, array $hierarchy, SchemaGeneratorInterface $schemaGenerator) {
    $getter = $type->addMethod('get' . ucfirst($config['name']));
    $setter = $type->addMethod('set' . ucfirst($config['name']));
    $setter->addParameter($config['name']);
    $types = [];
    foreach ($config['rangeIncludes'] as $range) {
      switch($hierarchy[$range]['type']) {
        case 'class':
          $types[] = $hierarchy[$range]['name'] . 'Interface';
          $types[] = $hierarchy[$range]['name'] . 'Interface[]';
          break;
        case 'primitive':
          $types[] = '\\' . $schemaGenerator->getDataTypes()[$range];
          break;
      }
    }
    if ($type->isInterface()) {
      if (count($types) > 0) {
        $types = join(' | ', $types);
        $getter->addComment($this->formatComment($config['comment']));
        $getter->addComment('@return ' . $types);
        $setter->addComment('@param ' . $types . ' $' . $config['name']);
        $setter->addComment('@return static');
      }
    } else {
      $property = $type->addProperty($config['name']);
      $property->setProtected();
      $getter->addBody($this->getGetterBody($config['name']));
      $setter->addBody($this->getSetterBody($config['name']));
    }
  }

  protected function getGetterBody($name) {
    return 'return $this->getProperty(\'' . $name . '\');';
  }

  protected function getSetterBody($name) {
    return join(PHP_EOL, [
      '$this->setProperty(\'' . $name . '\', $' . $name . ');',
      'return $this;',
    ]);
  }

  protected function formatComment($comment) {
    return (new UnicodeString($comment . PHP_EOL))
      ->wordwrap(80)
      ->toString();
  }

}
