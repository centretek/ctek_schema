<?php

namespace Drupal\ctek_schema\Schema;

use Carbon\Carbon;

abstract class SchemaBase implements \JsonSerializable {

  protected $properties = [];

  public function __construct() {
  }

  public static function create() {
    return new static();
  }

  protected function setProperty($name, $value) {
    $this->properties[$name] = $value;
  }

  protected function getProperty($name) {
    if (isset($this->properties[$name])) {
      return $this->properties[$name];
    }
    return NULL;
  }

  public function jsonSerialize():array {
    $fqClassName = explode('\\', static::class);
    $className = array_pop($fqClassName);
    $properties = [
        '@type' => $className,
      ] + $this->properties;
    foreach ($properties as &$property) {
      if ($property instanceof Carbon) {
        $property = $property->toIso8601ZuluString();
      }
    }
    return $properties;
  }

}
