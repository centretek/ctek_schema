<?php

namespace Drupal\ctek_schema\Schema;

use Carbon\Carbon;
use Drupal\Core\Plugin\PluginBase;

abstract class SchemaGeneratorBase extends PluginBase implements SchemaGeneratorInterface {

  public function getSchemaFiles(): array {
    return [
      drupal_get_path('module', 'ctek_schema') . '/data/schema.rdf',
      drupal_get_path('module', 'ctek_schema') . '/data/ext-health-lifesci.rdf',
    ];
  }

  public function getDataTypes(): array {
    return [
      'http://schema.org/Boolean' => 'bool',
      'http://schema.org/Date' => Carbon::class,
      'http://schema.org/DateTime' => Carbon::class,
      'http://schema.org/Time' => Carbon::class,
      'http://schema.org/Number' => 'float',
      'http://schema.org/Float' => 'float',
      'http://schema.org/Integer' => 'integer',
      'http://schema.org/Text' => 'string',
      'http://schema.org/CssSelectorType' => 'string',
      'http://schema.org/PronouncableText' => 'string',
      'http://schema.org/URL' => 'string',
      'http://schema.org/XPathType' => 'string',
    ];
  }

  public function getTargetClasses(): array {
    return [
      'http://schema.org/MedicalClinic' => [
        'name',
        'description',
        'address',
        'priceRange',
        'logo',
        'image',
        'url',
        'telephone',
        'areaServed',
        'hasMap',
        'openingHours',
        'memberOf',
        'medicalSpecialty',
        'geo',
      ],
      'http://schema.org/Hospital' => [
        'name',
        'description',
        'address',
        'priceRange',
        'logo',
        'image',
        'url',
        'telephone',
        'areaServed',
        'hasMap',
        'openingHours',
        'memberOf',
        'geo',
      ],
      'http://schema.org/Event' => [
        'name',
        'description',
        'image',
        'startDate',
        'endDate',
        'performer',
        'location',
      ],
      'http://schema.org/Place' => [
        'url',
        'aggregateRating',
      ],
      'http://schema.org/PostalAddress' => [
        'streetAddress',
        'addressLocality',
        'addressRegion',
        'postalCode',
        'addressCountry',
      ],
      'http://schema.org/GeoCoordinates' => [
        'latitude',
        'longitude',
      ],
      'http://schema.org/JobPosting' => [
        'datePosted',
        'hiringOrganization',
        'jobLocation',
        'title',
        'description',
        'url',
      ],
      'http://schema.org/Physician' => [
        'name',
        'telephone',
        'image',
        'url',
        'address',
        'medicalSpecialty',
        'aggregateRating',
      ],
      'http://schema.org/AggregateRating' => [
        'reviewCount',
      ],
      'http://schema.org/Rating' => [
        'ratingValue',
      ],
      'http://schema.org/MedicalOrganization' => [
      ],
    ];
  }

  public function getClassNamespace(): string {
    return ltrim($this->pluginDefinition['classNamespace'], '/');
  }

  public function getInterfaceNamespace(): string {
    return ltrim($this->pluginDefinition['interfaceNamespace'], '/');
  }

  protected function getBasePath() {
    return drupal_get_path('module', $this->pluginDefinition['provider']);
  }

  public function getClassPath(): string {
    return  $this->getBasePath() . '/' . ltrim($this->pluginDefinition['classPath'], '/');
  }

  public function getInterfacePath(): string {
    return $this->getBasePath() . '/' . ltrim($this->pluginDefinition['interfacePath'], '/');
  }

}
