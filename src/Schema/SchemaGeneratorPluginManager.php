<?php

namespace Drupal\ctek_schema\Schema;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\ctek_schema\Annotation\SchemaGenerator;
use Drupal\ctek_schema\Schema\SchemaGeneratorInterface;

/**
 * Model plugin manager.
 */
class SchemaGeneratorPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Schema',
      $namespaces,
      $module_handler,
      SchemaGeneratorInterface::class,
      SchemaGenerator::class
    );
    $this->alterInfo('schema_generator_info');
    $this->setCacheBackend($cache_backend, 'schema_generator__plugins');
  }

}
