<?php

namespace Drupal\ctek_schema\Schema;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface SchemaGeneratorInterface extends PluginInspectionInterface {

  public function getSchemaFiles() : array;

  public function getDataTypes() : array;

  public function getTargetClasses() : array;

  public function getClassNamespace() : string;

  public function getInterfaceNamespace() : string;

  public function getClassPath() : string;

  public function getInterfacePath() : string;

}
